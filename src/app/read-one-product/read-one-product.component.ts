import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { ProductService } from '../product.service';
import { Observable } from 'rxjs';
import { Product } from '../product';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-read-one-product',
  templateUrl: './read-one-product.component.html',
  styleUrls: ['./read-one-product.component.css'],
  providers: [ProductService]
})

export class ReadOneProductComponent implements OnChanges {

  @Output() show_read_products_event = new EventEmitter();
  @Input() product_id;

  product: Product;

  constructor(private productService: ProductService) {
  }

  readProducts() {
    this.show_read_products_event.emit({ title: "Read Product" });
  }

  ngOnChanges() {
    this.productService.readOneProduct(this.product_id)
      .subscribe(product =>
        // console.log(JSON.stringify(product))
        this.product = product
      );
  }

}
